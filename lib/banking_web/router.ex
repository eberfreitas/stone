defmodule BankingWeb.Router do
  use BankingWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :backoffice do
    plug BasicAuth, use_config: {:banking, :basic_auth}
  end

  scope "/", BankingWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/reports", BankingWeb do
    pipe_through [:browser, :backoffice]

    get "/", LogController, :index
  end

  scope "/api", BankingWeb do
    pipe_through :api

    resources "/users", UserController, only: [:create, :show], singleton: true
    get "/transactions/:id", TransactionController, :show
    post "/transactions/withdrawal", TransactionController, :withdrawal
    post "/transactions/deposit", TransactionController, :deposit
    post "/transactions/transfer", TransactionController, :transfer
  end
end
