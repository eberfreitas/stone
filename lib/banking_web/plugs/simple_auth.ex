defmodule BankingWeb.Plugs.SimpleAuth do
  @behaviour Plug

  import Ecto.Query, only: [from: 2]
  import Plug.Conn

  alias Banking.{Accounts.User, Repo}
  alias Plug.Conn.Status

  @impl Plug
  def init(_), do: []

  @impl Plug
  def call(conn, _) do
    token =
      case get_req_header(conn, "x-auth-token") do
        [] -> nil
        [token] -> token
      end

    case get_user(token) do
      :error -> unauthorized(conn)
      {:ok, user} -> assign(conn, :user, user)
    end
  end

  @doc false
  defp unauthorized(conn) do
    conn
    |> send_resp(401, Status.reason_phrase(401))
    |> halt()
  end

  @doc false
  defp get_user(nil), do: :error

  defp get_user(token) do
    query = from u in User, where: u.token == ^token, preload: [:account]

    case Repo.one(query) do
      nil -> :error
      user -> {:ok, user}
    end
  end
end
