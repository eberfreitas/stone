defmodule BankingWeb.TransactionView do
  use BankingWeb, :view
  alias BankingWeb.TransactionView

  def render("show.json", %{transaction: transaction}) do
    %{data: render_one(transaction, TransactionView, "transaction.json")}
  end

  def render("transaction.json", %{transaction: transaction}) do
    %{
      id: transaction.id,
      type: transaction.type,
      amount: transaction.amount,
      to: transaction.to,
      status: transaction.status,
      failure_reason: transaction.failure_reason
    }
  end
end
