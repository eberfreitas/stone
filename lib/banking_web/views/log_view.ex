defmodule BankingWeb.LogView do
  use BankingWeb, :view

  @doc false
  def report_name(period) do
    case period do
      :all -> "Todos"
      :year -> "Anual"
      :month -> "Mensal"
      :day -> "Diário"
      _ -> "Desconhecido"
    end
  end

  @doc false
  def date_display(:all, nil), do: "∞"

  def date_display(period, date) do
    day =
      date.day
      |> to_string()
      |> String.pad_leading(2, "0")

    month =
      date.month
      |> to_string()
      |> String.pad_leading(2, "0")

    case period do
      :year -> "#{date.year}"
      :month -> "#{month}/#{date.year}"
      :day -> "#{day}/#{month}/#{date.year}"
    end
  end

  @doc false
  def money(nil), do: "R$ 0.00"

  def money(amount) do
    amount
    |> Money.new()
    |> to_string()
  end

  @doc false
  def money_class(amount) do
    cond do
      amount > 0 -> "credit"
      amount < 0 -> "debit"
      amount == 0 -> "neutral"
    end
  end
end
