defmodule BankingWeb.UserView do
  use BankingWeb, :view

  alias BankingWeb.UserView

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{
      id: user.id,
      email: user.email,
      token: user.token,
      account: %{balance: user.account.balance}
    }
  end
end
