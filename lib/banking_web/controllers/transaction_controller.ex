defmodule BankingWeb.TransactionController do
  use BankingWeb, :controller

  alias Banking.Mail.{Email, Mailer}
  alias Banking.Transactions
  alias Banking.Transactions.{Queue, Transaction}

  plug BankingWeb.Plugs.SimpleAuth

  action_fallback BankingWeb.FallbackController

  def withdrawal(conn, %{"withdrawal" => withdrawal_params}) do
    with {:ok, %Transaction{} = transaction} <-
           Transactions.create_withdrawal(conn.assigns.user.account, withdrawal_params),
         %Transaction{} = transaction <- Queue.process(transaction) do
      if transaction.status == "success" do
        Email.withdrawal_email(conn.assigns.user, transaction)
        |> Mailer.deliver_later()
      end

      created_resp(conn, transaction)
    end
  end

  def deposit(conn, %{"deposit" => deposit_params}) do
    with {:ok, %Transaction{} = transaction} <-
           Transactions.create_deposit(conn.assigns.user.account, deposit_params),
         %Transaction{} = transaction <- Queue.process(transaction) do
      created_resp(conn, transaction)
    end
  end

  def transfer(conn, %{"transfer" => transfer_params}) do
    with {:ok, %Transaction{} = transaction} <-
           Transactions.create_transfer(conn.assigns.user.account, transfer_params),
         %Transaction{} = transaction <- Queue.process(transaction) do
      created_resp(conn, transaction)
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, %Transaction{} = transaction} <-
           Transactions.get_transaction!(conn.assigns.user.account, id) do
      render(conn, "show.json", transaction: transaction)
    end
  end

  defp created_resp(conn, transaction) do
    conn
    |> put_status(:created)
    |> put_resp_header("location", Routes.transaction_path(conn, :show, transaction))
    |> render("show.json", transaction: transaction)
  end
end
