defmodule BankingWeb.LogController do
  use BankingWeb, :controller

  alias Banking.Transactions

  def index(conn, %{"period" => period}) do
    period = String.to_atom(period)
    report_data = Transactions.report(period)

    render(conn, "index.html", period: period, report_data: report_data)
  end

  def index(conn, _params) do
    report_data = Transactions.report(:all)

    render(conn, "index.html", period: :all, report_data: report_data)
  end
end
