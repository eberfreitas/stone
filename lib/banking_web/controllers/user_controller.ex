defmodule BankingWeb.UserController do
  use BankingWeb, :controller

  alias Banking.Accounts
  alias Banking.Accounts.User

  plug BankingWeb.Plugs.SimpleAuth when action in [:show]

  action_fallback BankingWeb.FallbackController

  def create(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Accounts.create_user(user_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.user_path(conn, :show))
      |> render("show.json", user: user)
    end
  end

  def show(conn, _) do
    render(conn, "show.json", user: conn.assigns.user)
  end
end
