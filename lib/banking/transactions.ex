defmodule Banking.Transactions do
  @moduledoc """
  The Transactions context.
  """

  import Ecto.Query, warn: false

  alias Banking.Repo
  alias Banking.Accounts.Account
  alias Banking.Transactions.{Deposit, Log, Transaction, Transfer, Withdrawal}
  alias Ecto.{Changeset, Multi}

  @doc false
  def get_transaction!(%Account{} = account, id) do
    transaction = Repo.get!(Transaction, id)

    if transaction.account_id != account.id do
      {:error, :unauthorized}
    else
      {:ok, transaction}
    end
  end

  @doc false
  def create_transaction(%Account{} = account, attrs) do
    %Transaction{}
    |> Transaction.changeset(attrs)
    |> Changeset.put_assoc(:account, account)
    |> Repo.insert()
  end

  @doc false
  def create_withdrawal(%Account{} = account, attrs) do
    changeset = Withdrawal.changeset(%Withdrawal{}, attrs)

    case changeset.valid? do
      false ->
        {:error, changeset}

      true ->
        withdrawal = Changeset.apply_changes(changeset)
        attrs = %{amount: withdrawal.amount, type: "withdrawal"}
        create_transaction(account, attrs)
    end
  end

  @doc false
  def create_deposit(%Account{} = account, attrs) do
    changeset = Deposit.changeset(%Deposit{}, attrs)

    case changeset.valid? do
      false ->
        {:error, changeset}

      true ->
        deposit = Changeset.apply_changes(changeset)
        attrs = %{amount: deposit.amount, type: "deposit"}
        create_transaction(account, attrs)
    end
  end

  @doc false
  def create_transfer(%Account{} = account, attrs) do
    changeset = Transfer.changeset(%Transfer{}, attrs)

    case changeset.valid? do
      false ->
        {:error, changeset}

      true ->
        transfer = Changeset.apply_changes(changeset)
        attrs = %{amount: transfer.amount, to: transfer.to, type: "transfer"}
        create_transaction(account, attrs)
    end
  end

  @doc false
  def process_transaction(%Transaction{type: type, status: "pending"} = transaction) do
    case type do
      "withdrawal" -> process_withdrawal(transaction)
      "deposit" -> process_deposit(transaction)
      "transfer" -> process_transfer(transaction)
    end
  end

  @doc false
  def process_withdrawal(%Transaction{type: "withdrawal", status: "pending"} = transaction) do
    account =
      from(a in Account, where: a.id == ^transaction.account_id)
      |> Repo.one()

    if account.balance < transaction.amount do
      {:ok, transaction} =
        transaction
        |> Transaction.changeset(%{status: "failed", failure_reason: "Insufficient funds"})
        |> Repo.update()

      transaction
    else
      amount = transaction.amount * -1
      new_balance = account.balance + amount
      log_attrs = %{amount: amount}
      account_attrs = %{balance: new_balance}
      transaction_attrs = %{status: "success"}

      {:ok, %{transaction: transaction}} =
        Ecto.Multi.new()
        |> Multi.insert(:log, %Log{} |> Log.changeset(transaction, account, log_attrs))
        |> Multi.update(:account, account |> Account.changeset(account_attrs))
        |> Multi.update(:transaction, transaction |> Transaction.changeset(transaction_attrs))
        |> Repo.transaction()

      transaction
    end
  end

  @doc false
  def process_deposit(%Transaction{type: "deposit", status: "pending"} = transaction) do
    account =
      from(a in Account, where: a.id == ^transaction.account_id)
      |> Repo.one()

    amount = transaction.amount
    new_balance = account.balance + amount
    log_attrs = %{amount: amount}
    account_attrs = %{balance: new_balance}
    transaction_attrs = %{status: "success"}

    {:ok, %{transaction: transaction}} =
      Ecto.Multi.new()
      |> Multi.insert(:log, %Log{} |> Log.changeset(transaction, account, log_attrs))
      |> Multi.update(:account, account |> Account.changeset(account_attrs))
      |> Multi.update(:transaction, transaction |> Transaction.changeset(transaction_attrs))
      |> Repo.transaction()

    transaction
  end

  @doc false
  def process_transfer(%Transaction{type: "transfer", status: "pending"} = transaction) do
    source_account =
      from(a in Account, where: a.id == ^transaction.account_id)
      |> Repo.one()

    dest_account =
      from(a in Account, join: u in assoc(a, :user), where: u.email == ^transaction.to)
      |> Repo.one()

    if source_account.balance < transaction.amount do
      {:ok, transaction} =
        transaction
        |> Transaction.changeset(%{status: "failed", failure_reason: "Insufficient funds"})
        |> Repo.update()

      transaction
    else
      credit_amount = transaction.amount
      debit_amount = credit_amount * -1
      credit_balance = dest_account.balance + credit_amount
      debit_balance = source_account.balance + debit_amount
      credit_log_attrs = %{amount: credit_amount}
      debit_log_attrs = %{amount: debit_amount}
      credit_account_attrs = %{balance: credit_balance}
      debit_account_attrs = %{balance: debit_balance}
      transaction_attrs = %{status: "success"}

      {:ok, %{transaction: transaction}} =
        Multi.new()
        |> Multi.insert(
          :credit_log,
          %Log{} |> Log.changeset(transaction, dest_account, credit_log_attrs)
        )
        |> Multi.insert(
          :debit_log,
          %Log{} |> Log.changeset(transaction, source_account, debit_log_attrs)
        )
        |> Multi.update(:credit_account, dest_account |> Account.changeset(credit_account_attrs))
        |> Multi.update(:debit_account, source_account |> Account.changeset(debit_account_attrs))
        |> Multi.update(:transaction, transaction |> Transaction.changeset(transaction_attrs))
        |> Repo.transaction()

      transaction
    end
  end

  @doc false
  def report(by) do
    case by do
      :all -> report_all()
      :year -> report_year()
      :month -> report_month()
      :day -> report_day()
      _ -> :error
    end
    |> build_report()
  end

  @doc false
  def report_all() do
    query_credit =
      from l in Log, select: %{balance: sum(l.amount), date: nil}, where: l.amount > 0

    query_debit = from l in Log, select: %{balance: sum(l.amount), date: nil}, where: l.amount < 0
    query_balance = from l in Log, select: %{balance: sum(l.amount), date: nil}

    credit = Task.async(fn -> Repo.all(query_credit) end)
    debit = Task.async(fn -> Repo.all(query_debit) end)
    balance = Task.async(fn -> Repo.all(query_balance) end)

    {Task.await(credit), Task.await(debit), Task.await(balance)}
  end

  @doc false
  def report_year() do
    query_credit =
      from l in Log,
        select: %{balance: sum(l.amount), date: fragment("date_trunc('year', ?)", l.inserted_at)},
        where: l.amount > 0,
        order_by: [desc: fragment("date_trunc('year', ?)", l.inserted_at)],
        group_by: fragment("date_trunc('year', ?)", l.inserted_at)

    query_debit =
      from l in Log,
        select: %{balance: sum(l.amount), date: fragment("date_trunc('year', ?)", l.inserted_at)},
        where: l.amount < 0,
        order_by: [desc: fragment("date_trunc('year', ?)", l.inserted_at)],
        group_by: fragment("date_trunc('year', ?)", l.inserted_at)

    query_balance =
      from l in Log,
        select: %{balance: sum(l.amount), date: fragment("date_trunc('year', ?)", l.inserted_at)},
        order_by: [desc: fragment("date_trunc('year', ?)", l.inserted_at)],
        group_by: fragment("date_trunc('year', ?)", l.inserted_at)

    credit = Task.async(fn -> Repo.all(query_credit) end)
    debit = Task.async(fn -> Repo.all(query_debit) end)
    balance = Task.async(fn -> Repo.all(query_balance) end)

    {Task.await(credit), Task.await(debit), Task.await(balance)}
  end

  @doc false
  def report_month() do
    query_credit =
      from l in Log,
        select: %{balance: sum(l.amount), date: fragment("date_trunc('month', ?)", l.inserted_at)},
        where: l.amount > 0,
        order_by: [desc: fragment("date_trunc('month', ?)", l.inserted_at)],
        group_by: fragment("date_trunc('month', ?)", l.inserted_at)

    query_debit =
      from l in Log,
        select: %{balance: sum(l.amount), date: fragment("date_trunc('month', ?)", l.inserted_at)},
        where: l.amount < 0,
        order_by: [desc: fragment("date_trunc('month', ?)", l.inserted_at)],
        group_by: fragment("date_trunc('month', ?)", l.inserted_at)

    query_balance =
      from l in Log,
        select: %{balance: sum(l.amount), date: fragment("date_trunc('month', ?)", l.inserted_at)},
        order_by: [desc: fragment("date_trunc('month', ?)", l.inserted_at)],
        group_by: fragment("date_trunc('month', ?)", l.inserted_at)

    credit = Task.async(fn -> Repo.all(query_credit) end)
    debit = Task.async(fn -> Repo.all(query_debit) end)
    balance = Task.async(fn -> Repo.all(query_balance) end)

    {Task.await(credit), Task.await(debit), Task.await(balance)}
  end

  @doc false
  def report_day() do
    query_credit =
      from l in Log,
        select: %{balance: sum(l.amount), date: fragment("date_trunc('day', ?)", l.inserted_at)},
        where: l.amount > 0,
        order_by: [desc: fragment("date_trunc('day', ?)", l.inserted_at)],
        group_by: fragment("date_trunc('day', ?)", l.inserted_at)

    query_debit =
      from l in Log,
        select: %{balance: sum(l.amount), date: fragment("date_trunc('day', ?)", l.inserted_at)},
        where: l.amount < 0,
        order_by: [desc: fragment("date_trunc('day', ?)", l.inserted_at)],
        group_by: fragment("date_trunc('day', ?)", l.inserted_at)

    query_balance =
      from l in Log,
        select: %{balance: sum(l.amount), date: fragment("date_trunc('day', ?)", l.inserted_at)},
        order_by: [desc: fragment("date_trunc('day', ?)", l.inserted_at)],
        group_by: fragment("date_trunc('day', ?)", l.inserted_at)

    credit = Task.async(fn -> Repo.all(query_credit) end)
    debit = Task.async(fn -> Repo.all(query_debit) end)
    balance = Task.async(fn -> Repo.all(query_balance) end)

    {Task.await(credit), Task.await(debit), Task.await(balance)}
  end

  @doc false
  def build_report({credit, debit, balance}) do
    dates = Enum.map(balance, fn item -> item.date end)

    for date <- dates do
      %{
        date: date,
        credit: Enum.find(credit, %{balance: 0}, fn i -> i.date == date end).balance,
        debit: Enum.find(debit, %{balance: 0}, fn i -> i.date == date end).balance,
        balance: Enum.find(balance, %{balance: 0}, fn i -> i.date == date end).balance
      }
    end
  end
end
