defmodule Banking.Transactions.Deposit do
  use Ecto.Schema

  import Ecto.Changeset

  embedded_schema do
    field :amount, :integer
  end

  @doc false
  def changeset(deposit, attrs) do
    deposit
    |> cast(attrs, [:amount])
    |> validate_required([:amount])
    |> validate_number(:amount, greater_than: 0)
  end
end
