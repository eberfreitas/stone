defmodule Banking.Transactions.Transfer do
  use Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]

  alias Banking.Accounts.User

  embedded_schema do
    field :to, :string
    field :amount, :integer
  end

  @doc false
  def changeset(transfer, attrs) do
    transfer
    |> cast(attrs, [:amount, :to])
    |> validate_required([:amount, :to])
    |> validate_number(:amount, greater_than: 0)
    |> validate_format(:to, ~r/@/)
    |> validate_change(:to, &destination_exists(&1, &2))
  end

  defp destination_exists(_, to) do
    query = from u in User, where: u.email == ^to, select: count()

    case Banking.Repo.one(query) do
      0 -> [to: "destination account must exist"]
      _ -> []
    end
  end
end
