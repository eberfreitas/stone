defmodule Banking.Transactions.Transaction do
  use Ecto.Schema

  import Ecto.Changeset

  schema "transactions" do
    belongs_to :account, Banking.Accounts.Account
    has_many :logs, Banking.Transactions.Log
    field :amount, :integer
    field :failure_reason, :string
    field :to, :string
    field :status, :string
    field :type, :string

    timestamps()
  end

  @doc false
  def changeset(transaction, attrs) do
    transaction
    |> cast(attrs, [:type, :amount, :to, :status, :failure_reason])
    |> validate_required([:type, :amount])
    |> set_status()
  end

  defp set_status(changeset) do
    case get_field(changeset, :status) do
      nil ->
        changeset |> put_change(:status, "pending")

      _ ->
        changeset
    end
  end
end
