defmodule Banking.Transactions.Withdrawal do
  use Ecto.Schema

  import Ecto.Changeset

  embedded_schema do
    field :amount, :integer
  end

  @doc false
  def changeset(withdrawal, attrs) do
    withdrawal
    |> cast(attrs, [:amount])
    |> validate_required([:amount])
    |> validate_number(:amount, greater_than: 0)
  end
end
