defmodule Banking.Transactions.Log do
  use Ecto.Schema
  import Ecto.Changeset

  schema "logs" do
    belongs_to :transaction, Banking.Transactions.Transaction
    belongs_to :account, Banking.Accounts.Account
    field :amount, :integer

    timestamps()
  end

  @doc false
  def changeset(log, transaction, account, attrs) do
    log
    |> cast(attrs, [:amount])
    |> validate_required([:amount])
    |> put_assoc(:transaction, transaction)
    |> put_assoc(:account, account)
  end
end
