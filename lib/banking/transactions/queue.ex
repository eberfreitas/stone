defmodule Banking.Transactions.Queue do
  use GenServer

  alias Banking.Transactions

  def start_link(opts \\ []) do
    name = Keyword.get(opts, :name, Banking.Transactions.QueueServer)
    GenServer.start_link(__MODULE__, :ok, name: name)
  end

  def process(pid \\ Banking.Transactions.QueueServer, transaction) do
    GenServer.call(pid, {:process, transaction})
  end

  @impl true
  def init(:ok), do: {:ok, 0}

  @impl true
  def handle_call({:process, transaction}, _from, count) do
    transaction = Transactions.process_transaction(transaction)
    {:reply, transaction, count + 1}
  end
end
