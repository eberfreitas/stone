defmodule Banking.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false

  alias Banking.Repo
  alias Banking.Accounts.{Account, User}
  alias Banking.Transactions.{Log, Transaction}
  alias Ecto.Changeset

  @doc false
  def get_user!(id) do
    User |> preload(:account) |> Repo.get!(id)
  end

  @doc false
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Changeset.put_change(:token, token())
    |> Changeset.put_assoc(:account, %Account{
      balance: 1_000_00,
      transactions: [
        %Transaction{
          type: "deposit",
          status: "success",
          amount: 1_000_00,
          logs: [%Log{amount: 1_000_00}]
        }
      ]
    })
    |> Repo.insert()
  end

  @doc false
  def token() do
    :crypto.strong_rand_bytes(64) |> Base.encode64() |> binary_part(0, 64)
  end
end
