defmodule Banking.Accounts.Account do
  use Ecto.Schema

  import Ecto.Changeset

  schema "accounts" do
    belongs_to :user, Banking.Accounts.User
    field :balance, :integer, default: 1_000_00
    has_many :transactions, Banking.Transactions.Transaction
    has_many :logs, Banking.Transactions.Log

    timestamps()
  end

  @doc false
  def changeset(account, attrs) do
    account
    |> cast(attrs, [:balance])
    |> validate_number(:balance, greater_than: 0)
  end
end
