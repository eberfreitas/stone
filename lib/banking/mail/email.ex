defmodule Banking.Mail.Email do
  import Bamboo.Email

  alias Banking.Accounts.User
  alias Banking.Transactions.Transaction

  def base_email() do
    new_email()
    |> from(Application.fetch_env!(:banking, Banking.Mail.Mailer)[:from])
  end

  def withdrawal_email(%User{} = user, %Transaction{type: "withdrawal"} = transaction) do
    amount = Money.new(transaction.amount)

    body = """
    Olá!

    Você está recebendo este e-mail pois realizou umsa que no valor de:

    #{to_string(amount)}
    """

    base_email()
    |> to(user.email)
    |> subject("Saque realizado!")
    |> text_body(body)
  end
end
