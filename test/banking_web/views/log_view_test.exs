defmodule BankingWeb.LogViewTest do
  use BankingWeb.ConnCase, async: true

  import BankingWeb.LogView

  test "report name" do
    assert report_name(nil) == "Desconhecido"
    assert report_name(:all) == "Todos"
    assert report_name(:day) == "Diário"
  end

  test "date display" do
    assert date_display(:all, nil) == "∞"

    date = ~N[2018-01-01 00:00:00]

    assert date_display(:day, date) == "01/01/2018"
    assert date_display(:month, date) == "01/2018"
    assert date_display(:year, date) == "2018"
  end

  test "money display" do
    assert money(100_00) == "R$ 100.00"
  end

  test "money class" do
    assert money_class(-1) == "debit"
    assert money_class(1) == "credit"
    assert money_class(0) == "neutral"
  end
end
