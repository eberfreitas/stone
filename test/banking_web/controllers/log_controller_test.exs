defmodule BankingWeb.LogControllerTest do
  use BankingWeb.ConnCase

  @username Application.get_env(:banking, :basic_auth)[:username]
  @password Application.get_env(:banking, :basic_auth)[:password]

  setup %{conn: conn} do
    header_content = "Basic " <> Base.encode64("#{@username}:#{@password}")
    conn = conn |> put_req_header("authorization", header_content)
    {:ok, conn: conn}
  end

  test "GET /reports renders reports pages", %{conn: conn} do
    resp = conn |> get(Routes.log_path(conn, :index))
    assert html_response(resp, 200) =~ "Todos"

    resp = conn |> get(Routes.log_path(conn, :index, period: "month"))
    assert html_response(resp, 200) =~ "Mensal"
  end
end
