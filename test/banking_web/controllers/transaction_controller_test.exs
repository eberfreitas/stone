defmodule BankingWeb.TransactionControllerTest do
  use BankingWeb.ConnCase

  import Banking.Factory

  @user insert!(:user_with_account)

  setup %{conn: conn} do
    conn =
      conn
      |> put_req_header("accept", "application/json")
      |> put_req_header("x-auth-token", @user.token)

    {:ok, conn: conn}
  end

  test "POST /api/transactions/withdrawal returns transaction when data is valid", %{conn: conn} do
    conn = post(conn, Routes.transaction_path(conn, :withdrawal), withdrawal: %{amount: 500_00})
    assert %{"id" => _id, "status" => "success"} = json_response(conn, 201)["data"]
  end

  test "POST /api/transactions/deposit returns transaction when data is valid", %{conn: conn} do
    conn = post(conn, Routes.transaction_path(conn, :deposit), deposit: %{amount: 500_00})
    assert %{"id" => _id, "status" => "success"} = json_response(conn, 201)["data"]
  end

  test "POST /api/transactions/transfer returns transaction when data is valid", %{conn: conn} do
    post_conn =
      post(conn, Routes.transaction_path(conn, :transfer),
        transfer: %{amount: 500_00, to: @user.email}
      )

    assert %{"id" => id, "status" => "success"} = json_response(post_conn, 201)["data"]

    get_conn = get(conn, Routes.transaction_path(conn, :show, id))

    assert %{"id" => id, "amount" => 50000, "status" => "success"} =
             json_response(get_conn, 200)["data"]
  end
end
