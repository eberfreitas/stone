defmodule BankingWeb.UserControllerTest do
  use BankingWeb.ConnCase

  alias Banking.Accounts

  @create_attrs %{email: "johndoe@yolo.com"}
  @invalid_attrs %{email: nil}

  def fixture(:user) do
    {:ok, user} = Accounts.create_user(@create_attrs)
    user
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "POST /api/users create user" do
    test "renders user when data is valid", %{conn: conn} do
      post_conn = post(conn, Routes.user_path(conn, :create), user: @create_attrs)
      response = json_response(post_conn, 201)["data"]
      assert %{"id" => id} = response

      get_conn =
        conn
        |> put_req_header("x-auth-token", response["token"])
        |> get(Routes.user_path(conn, :show))

      assert %{
               "id" => id,
               "email" => "johndoe@yolo.com",
               "token" => token,
               "account" => account
             } = json_response(get_conn, 200)["data"]
    end

    test "POST /api/users renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :create), user: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end
end
