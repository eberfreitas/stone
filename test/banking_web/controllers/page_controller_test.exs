defmodule BankingWeb.PageControllerTest do
  use BankingWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "R$ 1.000,00"
  end
end
