defmodule Banking.Factory do
  alias Banking.Repo

  def build(:account) do
    %Banking.Accounts.Account{balance: 1_000_00}
  end

  def build(:user_with_account) do
    %Banking.Accounts.User{
      email: "john#{System.unique_integer([:positive])}@example.com",
      token: Banking.Accounts.token(),
      account: build(:account)
    }
  end

  def build(factory_name, attrs \\ []) do
    factory_name |> build() |> struct(attrs)
  end

  def insert!(factory_name, attrs \\ []) do
    build(factory_name, attrs) |> Repo.insert!()
  end
end
