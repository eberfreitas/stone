defmodule Banking.Mail.EmailTest do
  use ExUnit.Case
  use Bamboo.Test

  alias Banking.Mail.{Email, Mailer}
  alias Banking.Accounts.User
  alias Banking.Transactions.Transaction

  test "withdrawal email and sending" do
    user = %User{email: "johndoe@example.com"}
    transaction = %Transaction{type: "withdrawal", amount: 500_00}
    email = Email.withdrawal_email(user, transaction)

    assert email.from == Application.fetch_env!(:banking, Banking.Mail.Mailer)[:from]
    assert email.to == "johndoe@example.com"
    assert email.subject == "Saque realizado!"
    assert email.text_body =~ "R$ 500.00"

    email |> Mailer.deliver_now()

    assert_delivered_email(email)
  end
end
