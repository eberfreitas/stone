defmodule Banking.TransactionsTest do
  use Banking.DataCase

  import Banking.Factory

  alias Banking.Transactions
  alias Banking.Transactions.Transaction

  @valid_attrs %{
    amount: 100_00,
    status: "pending",
    type: "withdrawal"
  }

  @invalid_attrs %{amount: nil, status: nil, type: nil}

  def user_fixture() do
    insert!(:user_with_account)
  end

  def transaction_fixture(attrs \\ %{}) do
    attrs = Enum.into(attrs, @valid_attrs)
    user = user_fixture()
    {:ok, transaction} = Transactions.create_transaction(user.account, attrs)
    transaction
  end

  test "get_transaction!/2 returns the transaction with given id" do
    transaction = transaction_fixture()
    {:ok, t} = Transactions.get_transaction!(transaction.account, transaction.id)
    assert t.id == transaction.id
  end

  test "get_transaction!/2 with invalid account returns error tuple" do
    transaction = transaction_fixture()

    assert {:error, :unauthorized} =
             Transactions.get_transaction!(%Banking.Accounts.Account{id: 0}, transaction.id)
  end

  test "create_transaction/2 with valid data creates a transaction" do
    user = user_fixture()

    assert {:ok, %Transaction{} = transaction} =
             Transactions.create_transaction(user.account, @valid_attrs)

    assert transaction.amount == 100_00
    assert transaction.status == "pending"
    assert transaction.type == "withdrawal"
  end

  test "create_transaction/2 with invalid data returns error changeset" do
    user = user_fixture()

    assert {:error, %Ecto.Changeset{}} =
             Transactions.create_transaction(user.account, @invalid_attrs)
  end

  test "create_withdrawal/2 returns transaction" do
    user = user_fixture()
    assert {:ok, %Transaction{}} = Transactions.create_withdrawal(user.account, %{amount: 100_00})
  end

  test "create_withdrawal/2 returns error" do
    user = user_fixture()
    assert {:error, %Ecto.Changeset{}} = Transactions.create_withdrawal(user.account, %{})
  end

  test "create_transfer/2 returns transaction" do
    user = user_fixture()

    assert {:ok, %Transaction{}} =
             Transactions.create_transfer(user.account, %{amount: 100_00, to: user.email})
  end

  test "create_transfer/2 returns error" do
    user = user_fixture()

    assert {:error, %Ecto.Changeset{}} =
             Transactions.create_transfer(user.account, %{to: "johndoe@dontexists.com"})
  end

  test "process_withdrawal/1 returns failed transaction" do
    transaction = transaction_fixture(%{amount: 10_000_00})
    assert %Transaction{status: "failed"} = Transactions.process_withdrawal(transaction)
  end

  test "process_withdrawal/1 returns success transaction" do
    transaction = transaction_fixture(%{amount: 100_00})
    assert %Transaction{status: "success"} = Transactions.process_withdrawal(transaction)
  end

  test "process_deposit/1 returns success transaction" do
    transaction = transaction_fixture(%{type: "deposit", amount: 100_00})
    assert %Transaction{status: "success"} = Transactions.process_deposit(transaction)
  end

  test "process_transfer/1 returns failed transaction" do
    user = user_fixture()
    transaction = transaction_fixture(%{type: "transfer", to: user.email, amount: 10_000_00})
    assert %Transaction{status: "failed"} = Transactions.process_transfer(transaction)
  end

  test "process_transfer/1 returns success transaction" do
    user = user_fixture()
    transaction = transaction_fixture(%{type: "transfer", to: user.email, amount: 100_00})
    assert %Transaction{status: "success"} = Transactions.process_transfer(transaction)
  end
end
