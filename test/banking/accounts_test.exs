defmodule Banking.AccountsTest do
  use Banking.DataCase

  alias Banking.Accounts
  alias Banking.Accounts.User

  @valid_attrs %{email: "johndoe@example.com"}
  @invalid_attrs %{email: "johndoe"}

  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(@valid_attrs)
      |> Accounts.create_user()

    user
  end

  test "get_user!/1 returns the user with given id" do
    user = user_fixture()
    fetched = Accounts.get_user!(user.id)
    assert fetched.id == user.id
  end

  test "create_user/1 with valid data creates a user" do
    assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
    assert user.email == "johndoe@example.com"
    assert user.token != nil
  end

  test "create_user/1 with invalid data returns error changeset" do
    assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
  end

  test "token/0 returns 64 bytes token binary" do
    token = Accounts.token()

    assert byte_size(token) == 64
  end
end
