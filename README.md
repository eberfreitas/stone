# Stone Banking

**Sistema de baking feito para o
[Desafio Backend Stone](https://gist.github.com/thulio/e021378b27ff471795e37ba5a5b73539)**

## Instalação

O projeto foi feito para rodar dentro de containers Docker. Para iniciar, clone
este repositório. Em seguida, crie um arquivo `.env`. Você pode usar o arquivo
`.env.sample` como base. Depois basta iniciar nossos containers:

```
$ docker-compose up -d
```

O serviço deverá estar acessível no endereço `http://localhost:4000`. Antes de
acessá-lo, verifique se o serviço já está devidamente levantado no Docker
verificando os logs com o seguinte comando:

```
$ docker-compose logs -f
```

Procure pela linha que diz:

```
[info] Access BankingWeb.Endpoint at http://localhost:4000
```

Toda a **documentação sobre como interagir com a API** pode ser lida acessando o
endereço do sistema.

### Seeds

O sistema já vem com uma série de dados pré-definidos. Para inserir estes dados,
após a inicialização dos containers execute este comando:

```
$ docker-compose exec app mix run priv/repo/seeds.exs
```

### Desenvolvendo com Docker

Por padrão nosso `Dockerfile` faz uma cópia do projeto para o container. Para
desenvolver é necessário mapear nosso projeto através de um volume para dentro
do container. Isso pode ser feito criando um arquivo `docker-compose.override.yml`
e definindo um volume para o service `app`. Exemplo:

```
version: '3.7'

services:
  app:
    volumes:
      - "./:/app"
```

É possível alterar ou adicionar outras configurações usando o override. Para
mais informações leia a [documentação do Docker](https://docs.docker.com/compose/extends/)
sobre como extender as configurações do Compose.

### Testes

Para executar os testes basta usar o seguinte comando:

```
$ docker-compose exec app mix test
```

### Relatórios

Este sistema possui uma área de relatórios. É possível acessá-la clicando no
botão "RELATÓRIOS" ao final da página principal ou no endereço `/reports`. Para
acessar os relatórios você terá que informar um nome de usuário e senha. Eles
estão definidos no arquivo `.env` através das chaves `BO_USER` e `BO_PASSWORD`.

# Considerações

O objetivo deste projeto era criar um simples sistema bancário com as seguintes
regras:

  * Criação de usuários;
  * O usuário ao se cadastrar já ganha R$ 1.000,00;
  * Saques;
  * Transferências;
  * Envio de e-mail ao realizar saque;
  * Relatório com o total transacionaldo por dia, mês, ano e total.

## Contas

A estrutura dos usuários foi separada em duas tabelas: `users` e `accounts`. Em
`users` temos apenas as informações necessárias para o cadastro e login do
usuário. Ao se cadastrar é gerado um `token` que posteriormente deve ser
utilizado para realizar requisições autenticadas. Ao mesmo tempo também é criado
um registro em `accounts`, tabela responsável por solidificar a situação do
saldo do usuário.

## Transações

As movimentações em si acontecem com auxílio das tabelas `transactions` e
`logs`. Para cada tipo de transação: saque, transferência e depósito, é gerado
um registro em `transactions` com a sua situação que pode ser:

  * pending (ainda não foi processada)
  * success (processada com sucesso)
  * failed (houve problemas para processar a transação)

Quando uma transação é processada com sucesso são gravados registros sobre as
movimentações na tabela `logs` criando referências à transação e à conta afetada.
No caso de um saque de R$ 100.00, o log vai guardar a informação de que houve
movimentação de `-10000`. De forma semelhante, no caso de uma transferência,
são criados dois registros em `logs` ambos apontando para a mesma transação
mas informando movimentações diferentes: uma movimentação de crédito para a
conta destino e outra de débito para a conta de origem.

**Nota**

Todos os números são inteiros de forma que o valor de **R$ 1.000,00** é
representado internamente como `100000`.

Logo após o registro dos logs o saldo da conta é alterado de acordo com a nova
realidade.

## Fluxo das transações

Quando uma transação é gerada, de imediato ela tem a situação definida como
**pendente**. Imediatamente ela é entregue a um `GenServer` que vai realizar o
processamento desta transação. A utilização do `GenServer` garante que as
transações serão processadas na ordem em que o servidor recebe as mensagens
evitando, por exemplo, que duas transferências vejam um mesmo saldo e o balanço
fique incorreto posteriormente.

## Autenticação

A autenticação das requisições à API foi realizada com um simples `Plug` chamado
`SimpleAuth` que verifica a presença do header `X-Auth-Token` e consultando o
usuário que possui este token. Em seguida os dados deste usuário são adicionados
ao `conn.assigns` e disponível para as ações dos controllers.

## Envio de e-mails

O envio de e-mails é feito através da lib `Bamboo` e por padrão o adapter
utilizado (`Bamboo.LocalAdapter`) apenas loga localmente que o e-mail foi
enviado. É possível modificar o adapter e utilizar outros sistemas. Para saber
mais basta ler a documentação da lib [aqui](https://hexdocs.pm/bamboo/readme.html#available-adapters).
No momento e-mail é enviado apenas no ato de um saque.

## Problemas e oportunidades

  * Os relatórios não possuem paginação;
  * Idealmente não seria usado um `GenServer` para enfileirar as transações já
    que, sendo um processo só, acabaria por se tornar um bottleneck no sistema;
  * Não é possível editar ou remover usuários;
  * O sistema possui um sistema de autenticação simples e rudimentar;
  * Os testes foram desenvolvidos a posteriori pela natureza explorativa da
    tarefa. Idealmente seria utilizado TDD.
