# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Banking.Repo.insert!(%Banking.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

Banking.Repo.insert!(%Banking.Accounts.User{
  email: "johndoe@example.com",
  token: "oFPP89aqxxiBsxfevwl1WMxCjlqAZgD+W2+B9nPY0ZMTI5IMnQq4R5DCxh3t+0v5",
  account: %Banking.Accounts.Account{balance: 1_000_00},
  inserted_at: ~N[2018-01-01 00:00:00],
  updated_at: ~N[2018-01-01 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Transaction{
  account_id: 1,
  amount: 1_000_00,
  status: "success",
  type: "deposit",
  inserted_at: ~N[2018-01-01 00:00:00],
  updated_at: ~N[2018-01-01 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Log{
  transaction_id: 1,
  account_id: 1,
  amount: 1_000_00,
  inserted_at: ~N[2018-01-01 00:00:00],
  updated_at: ~N[2018-01-01 00:00:00]
})

Banking.Repo.insert!(%Banking.Accounts.User{
  email: "janedoe@example.com",
  token: "F8/rhMYcDXhGac5d6LKwbfVjeAtElVpoZmCD285KuRfzGDmfWkOuRgbOB61bT63p",
  account: %Banking.Accounts.Account{balance: 1_000_00},
  inserted_at: ~N[2018-01-01 00:00:00],
  updated_at: ~N[2018-01-01 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Transaction{
  account_id: 2,
  amount: 1_000_00,
  status: "success",
  type: "deposit",
  inserted_at: ~N[2018-01-01 00:00:00],
  updated_at: ~N[2018-01-01 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Log{
  transaction_id: 2,
  account_id: 2,
  amount: 1_000_00,
  inserted_at: ~N[2018-01-01 00:00:00],
  updated_at: ~N[2018-01-01 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Transaction{
  account_id: 1,
  amount: 100_00,
  status: "success",
  type: "withdrawal",
  inserted_at: ~N[2018-06-01 00:00:00],
  updated_at: ~N[2018-06-01 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Log{
  transaction_id: 3,
  account_id: 1,
  amount: -100_00,
  inserted_at: ~N[2018-06-01 00:00:00],
  updated_at: ~N[2018-06-01 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Transaction{
  account_id: 2,
  amount: 500_00,
  status: "success",
  to: "johndoe@example.com",
  type: "transfer",
  inserted_at: ~N[2019-01-01 00:00:00],
  updated_at: ~N[2019-01-01 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Log{
  transaction_id: 4,
  account_id: 2,
  amount: -500_00,
  inserted_at: ~N[2019-01-01 00:00:00],
  updated_at: ~N[2019-01-01 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Log{
  transaction_id: 4,
  account_id: 1,
  amount: 500_00,
  inserted_at: ~N[2019-01-01 00:00:00],
  updated_at: ~N[2019-01-01 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Transaction{
  account_id: 2,
  amount: 50_00,
  status: "success",
  type: "withdrawal",
  inserted_at: ~N[2019-06-01 00:00:00],
  updated_at: ~N[2019-06-01 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Log{
  transaction_id: 5,
  account_id: 2,
  amount: -50_00,
  inserted_at: ~N[2019-06-01 00:00:00],
  updated_at: ~N[2019-06-01 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Transaction{
  account_id: 1,
  amount: 350_00,
  status: "success",
  to: "janedoe@example.com",
  type: "transfer",
  inserted_at: ~N[2019-08-01 00:00:00],
  updated_at: ~N[2019-08-01 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Log{
  transaction_id: 6,
  account_id: 1,
  amount: -350_00,
  inserted_at: ~N[2019-08-01 00:00:00],
  updated_at: ~N[2019-08-01 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Log{
  transaction_id: 6,
  account_id: 2,
  amount: 350_00,
  inserted_at: ~N[2019-08-01 00:00:00],
  updated_at: ~N[2019-08-01 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Transaction{
  account_id: 2,
  amount: 200_00,
  status: "success",
  type: "deposit",
  inserted_at: ~N[2019-08-15 00:00:00],
  updated_at: ~N[2019-08-15 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Log{
  transaction_id: 7,
  account_id: 2,
  amount: 200_00,
  inserted_at: ~N[2019-08-15 00:00:00],
  updated_at: ~N[2019-08-15 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Transaction{
  account_id: 1,
  amount: 50_00,
  status: "success",
  type: "withdrawal",
  inserted_at: ~N[2019-08-18 00:00:00],
  updated_at: ~N[2019-08-18 00:00:00]
})

Banking.Repo.insert!(%Banking.Transactions.Log{
  transaction_id: 8,
  account_id: 1,
  amount: -50_00,
  inserted_at: ~N[2019-08-18 00:00:00],
  updated_at: ~N[2019-08-18 00:00:00]
})
