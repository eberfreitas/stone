defmodule Banking.Repo.Migrations.CreateAccounts do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :user_id, references(:users, on_delete: :nothing)
      add :balance, :integer, default: 1_000_00

      timestamps()
    end

    create index(:accounts, [:user_id])
  end
end
