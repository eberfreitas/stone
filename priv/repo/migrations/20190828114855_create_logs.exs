defmodule Banking.Repo.Migrations.CreateLogs do
  use Ecto.Migration

  def change do
    create table(:logs) do
      add :amount, :integer
      add :transaction_id, references(:transactions, on_delete: :nothing)
      add :account_id, references(:accounts, on_delete: :nothing)

      timestamps()
    end

    create index(:logs, [:transaction_id])
    create index(:logs, [:account_id])
  end
end
