defmodule Banking.Repo.Migrations.CreateTransactions do
  use Ecto.Migration

  def change do
    create table(:transactions) do
      add :account_id, references(:accounts, on_delete: :nothing)
      add :type, :string
      add :amount, :integer
      add :to, :text
      add :status, :string
      add :failure_reason, :string

      timestamps()
    end

    create index(:transactions, [:account_id])
  end
end
