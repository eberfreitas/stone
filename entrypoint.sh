#!/bin/sh
# Docker entrypoint script.

# Wait until Postgres is ready
while ! pg_isready -q -h $DB_HOST -p $DB_PORT -U $DB_USER
do
  echo "$(date) - waiting for database to start"
  sleep 2
done

cd assets && npm install && cd ..
mix deps.get
mix ecto.create
mix ecto.migrate
mix phx.server